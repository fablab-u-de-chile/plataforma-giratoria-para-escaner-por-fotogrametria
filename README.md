# Plataforma giratoria para fotogrametria.
[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

Plataforma de giro automatizado para fotografiar algún objeto que se pose sobre su superficie .
controlando el ángulo de giro y el tiempo de detención por cada uno de ellos , permitiendo ser ajustado según la fotografía que se le tomara a la pieza.

<img src="img/Captura_de_Pantalla_2021-06-23_a_la_s__14.05.16.png/"
     alt="render_iso"
     height="400">

## Componentes

| Lista de materiales         | Cantidad | Enlace proveedor* |
| ---------------------------:| --- |:-----|
| Motor nema 17               | 1  | [Motor nema](https://www.cimech3d.cl/producto/motor-stepper-bipolar-nema-17-41n-cm-cable-removible/)|
| Driver nema 17              | 1  | [Driver nema 17](https://afel.cl/producto/controlador-de-motores-paso-a-paso-driver-drv8825/)|
| Polea dentada GT2-6 20 dientes        |  1 | [Polea dentada gt2](https://www.cimech3d.cl/producto/polea-dentada-gt2-20-6mm/)|
| Correa dentada GT2-6 300mm    |  1  | [Correa dentada gt2](https://www.cimech3d.cl/producto/correa-cerrada-gt2-6-300mm-refuerzo-fibra-de-vidrio/)|
| Shield driver A4988/DRV8825 | 1   | [Shield driver motor](https://www.cimech3d.cl/producto/modulo-shield-para-driver-a4988-drv8825/)|
| Fuente de poder 9V DC  2.1mm  |  1  |     |
| Power Jack  2.1mm           | 1   |       |
| Interruptor DC              | 1 |      |
| Shield TFT 2.4″ Display     | 1 | [Pantalla touch](https://afel.cl/producto/shield-tft-pantalla-2-4-display-para-arduino/)|
| Arduino UNO                 |  1  | [Arduino UNO](https://afel.cl/producto/placa-uno-compatible-con-arduino-cable-usb/)|
| Perno parker plano M3 L10mm |     |    |
| Perno parker M3 L25mm       |     |    |
| Perno parker M3 L5mm        |     |    |
| Rodamiento axial OD 42mm    | 1 | [Rodamiento axial](https://www.mcmaster.com/https%3A%2F%2Fwwwmcmastercom%2Fbearings%2Fball-bearings/system-of-measurement~metric/thrust-ball-bearings-8/od~42mm/)|
| Rodamiento OD 22mm W 6mm    | 1 | [Rodamiento radial](https://www.mcmaster.com/bearings/system-of-measurement~metric/bearing-trade-number~6900h-2rs/)|

*Referencias revisadas en diciembre de 2021.

## Fabricación

Impresion 3d FDM.
Material Petg.

### Caracteristicas de impresión

- Nuzzle 0.8mm.
- Altura de capa 0.32mm.
- Primetros 10.
- Densidad relleno 100%.
- Temperatura impresión 235°C.
- Temperatura superficie de construcción 87°C.
- Flujo 90%.
- Velocidad 60mm/s.
- Velocidad capa inicial 7mm/s.
- Distancia de retracción 5mm.
- Velocidad de retracción 75mm/s.
- Soportes.
- Borde 3mm.

## Ensamblaje

### Despiece

<img src="img/despiece.png/"
     alt="render_iso"
     height="600">

| Número | Nombre |
| ------:| ------:|
|1       |        |
|2       |        |
|3       |        |
|4       |        |
|5       |        |
|6       |        |
|7       |        |
|8       |        |
|9       |        |
|10      |        |

### Diagrama electrónico

### Código Arduino

El firmware para el Arduino UNO que controla la plataforma giratoria lo puedes encontrar en este archivo -> [plataforma_giratoria.ino](src/plataforma_giratoria/plataforma_giratoria.ino).

Para compilar el código en el Arduino IDE necesitas las siguientes librerías externas en sus respectivas versiones:
 * Adafuit GFX Library v1.10.10
 * Adafruit TouchScreen Library v1.1.1
 * MCUfriend_kvb Library from https://github.com/prenticedavid/MCUFRIEND_kbv

Las tres liberías las puedes encontrar en la carpeta [src/libraries](src/libraries).

## Ejemplo fotogrametria

Con la plataforma se tomaron 350 fotografías de una roca de Calcosina, 182 de la parte inferior de la roca y 168 de la parte superior. Puedes descargar las imagenes desde el siguiente [enlace](https://drive.google.com/drive/folders/1NbWMahmC8LcXAjyWE7CZ6_rJ40srizSp?usp=sharing).

Utilizando el software Metashape se generó el sólido que puedes ver en el siguiente video:

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/7WK7bet_vlo/0.jpg)](https://www.youtube.com/watch?v=7WK7bet_vlo)

También existen software de fotogrametría que son de código abierto y gratuitos, como [Meshroom](https://alicevision.org/#meshroom) que funciona con el framework de código abierto para fotogrametría [AliceVision](https://alicevision.org/).


## Licencia

[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
